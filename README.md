# Rust on JVM playground

## Get started

1. Install [rust](https://www.rust-lang.org/learn/get-started)
2. Install wasm target for rust: 
    ```shell
    rustup target add wasm32-unknown-unknown
    ```
3. Build the wasm file: 
    ```shell
    cd mozgovina && cargo build --target wasm32-unknown-unknown --release
    ```
4. Build ktwasm:
    ```shell 
    cd ktwasm && ./mvnw install -DskipTests
    ```
5. Run the Wasm host app:
    ```shell
    cd mozgoread && ./gradlew run
    ```
   
You can now run the kotlin app `mozgoread`, which will use the
functions defined in the rust library
