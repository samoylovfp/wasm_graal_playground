use std::{
    borrow::BorrowMut,
    cell::RefCell,
    sync::{Mutex, Once},
};

use once_cell::sync::OnceCell;
use serde::Serialize;
use typeshare_annotation::typeshare;

#[cfg(target_arch = "wasm32")]
use lol_alloc::{FreeListAllocator, LockedAllocator};

#[cfg(target_arch = "wasm32")]
#[global_allocator]
static ALLOCATOR: LockedAllocator<FreeListAllocator> = LockedAllocator::new(FreeListAllocator::new());

#[derive(Serialize)]
#[typeshare]
pub struct Mozgovina {
    name: String,
    age: i32,
}

pub static mut RESULT_BUF: Vec<u8> = vec![];
pub static mut RESULT_PTR: [usize; 2] = [0, 0];

#[no_mangle]
pub extern "C" fn mozg(tupaya: i32) -> *const usize {
    let result = match tupaya {
        0 => Mozgovina {
            name: "Umnaya".to_string(),
            age: 500,
        },
        _ => Mozgovina {
            name: "Tupaya".to_string(),
            age: 5,
        },
    };
    unsafe {
        RESULT_BUF.clear();
        rmp_serde::encode::write_named(&mut RESULT_BUF, &result).unwrap();
        RESULT_PTR[0] = RESULT_BUF.as_ptr() as usize;
        RESULT_PTR[1] = RESULT_BUF.len();
        RESULT_PTR.as_ptr()
    }
}
