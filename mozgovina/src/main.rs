use std::slice;
use mozgovina::mozg;

fn main() {
    let ptr = mozg(0);
    let buf_slice: &[usize] = unsafe { slice::from_raw_parts(ptr.cast(), 2) };
    unsafe {
        println!(
            "{:X?}",
            slice::from_raw_parts(buf_slice[0] as *const u8, buf_slice[1])
        );
    }
}
