import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.kotlinModule
import de.cprohm.ktwasm.api.*
import de.cprohm.ktwasm.base.WasmValue
import org.msgpack.jackson.dataformat.MessagePackFactory
import java.io.File
import java.io.Serializable
import kotlin.system.measureNanoTime

data class Mozgovina(
    var name: String,
    var age: Int
) : Serializable

fun main() {
    val module = parseModule(File("../mozgovina/target/wasm32-unknown-unknown/release/mozgovina.wasm"))
    val calculateMozgFn = module.lookupFunction("mozg", null)
    val mapper = ObjectMapper(MessagePackFactory())
    mapper.registerModule(kotlinModule())

    (0..10_000).map {
        measureNanoTime {
            getMozg(module, 0, calculateMozgFn, mapper)
            getMozg(module, 1, calculateMozgFn, mapper)
        }
    }.drop(1000).average().also {
        print("Average time $it nanos")
    }

}

fun getMozg(
    module: Namespace,
    tupota: Int,
    calculateMozgFn: FunctionRef,
    mapper: ObjectMapper
): Mozgovina {
    val mozgovinaResultPtr = calculateMozgFn.call(listOf(WasmValue.wrap(tupota)))
    val memory = module.lookupMemory("memory", 0, 0)
    val mozgBufPtr = memory.loadI32(mozgovinaResultPtr.toI32(), 0)
    val mozgBufLen = memory.loadI32(mozgovinaResultPtr.toI32(), 4)
    val mozgBytes = memory.data.sliceArray(mozgBufPtr until mozgBufPtr + mozgBufLen)

    return mapper.readValue(mozgBytes, Mozgovina::class.java)
}
